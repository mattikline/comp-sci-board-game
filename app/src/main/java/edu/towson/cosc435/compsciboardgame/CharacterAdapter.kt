package edu.towson.cosc435.compsciboardgame


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import edu.towson.cosc435.compsciboardgame.interfaces.ICharController

import kotlinx.android.synthetic.main.character.view.*

class CharacterAdapter(val controller: ICharController): RecyclerView.Adapter<CharacterViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.character, parent, false)
        val holder = CharacterViewHolder(view)

        // Listener for which character is selected
        holder.itemView.charBtn.setOnClickListener {
            val position = holder.adapterPosition
            controller.handleSelect(position)
        }

        return holder
    }

    override fun getItemCount(): Int {
        return controller.chars.getCount()
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        val character = controller.chars.getChar(position)
        // Displays the info of the characters in the recyclerview
        holder.name?.text = character.name
        holder.image?.setImageResource(character.image)
        holder.description?.text = character.description
    }
}

class CharacterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    // Values that get used in onBindViewHolder above
    val name: TextView? = itemView.findViewById(R.id.charName)
    val image: ImageView? = itemView.findViewById(R.id.charImg)
    val description:TextView? = itemView.findViewById(R.id.charTxt)
}