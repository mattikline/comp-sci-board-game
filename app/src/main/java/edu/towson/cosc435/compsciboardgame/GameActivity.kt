package edu.towson.cosc435.compsciboardgame

import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import edu.towson.cosc435.compsciboardgame.fragments.QuestionFragment
import kotlinx.android.synthetic.main.activity_game.*

class GameActivity : AppCompatActivity(), QuestionFragment.QuestionDialogListener {
    // Keeps track of the tile the player is on, always starts on 1
    private var onTile = 1

    // Keeps track of the location the player's character is at
    private var locationX = 0f
    private var locationY = 0f

    // Keeps track of coins earned from correct answers
    private var coinsEarned = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        // Gets resource Id of the image of the selected board, sets the background as that board
        boardBackground.setImageResource(intent.getIntExtra("Background", R.drawable.grasspattern))

        // Gets resource Id of the image of the selected character, sets the character as that image
        characterAvatar.setImageResource(intent.getIntExtra("Character", R.drawable.adalovelace))

        // Listener for the question button, which is what progresses the game
        questionBtn.setOnClickListener { showQuestionDialog() }
    }

    private fun movePlayer() {
        // Gets a random number from 1 to 6 to move the player, like dice
        val spacesToMove = (1..6).random()

        // Just checking if the player is moving 1 space or not so it doesn't say something dumb like "Moving 1 spaces"
        if (spacesToMove == 1)
            Toast.makeText(this@GameActivity, "The answer is correct! Moving $spacesToMove space", Toast.LENGTH_LONG).show()
        else
            Toast.makeText(this@GameActivity, "The answer is correct! Moving $spacesToMove spaces", Toast.LENGTH_LONG).show()

        // For loop that iterates spacesToMove times to move the player to each next tile
        for (i in 1..spacesToMove) {

            // The player will be moving down the screen until they reach tile 9
            if (onTile < 9) {
                ObjectAnimator.ofFloat(characterAvatar, "translationY", locationY, locationY + 160f).apply {
                    duration = 250
                    start()
                }
                // Updates the location of the player vertically
                locationY += 160f
                Log.d(TAG, "Moved down")
            }

            // The player will be moving right while they're on tiles 9 through 11
            else if (onTile in 9..11) {
                ObjectAnimator.ofFloat(characterAvatar, "translationX", locationX, locationX + 288f).apply {
                    duration = 250
                    start()
                }
                // Updates the location of the player horizontally
                locationX += 288f
                Log.d(TAG, "Moved right")
            }

            // The player will be moving up while they're on tiles 12 through 19
            else if (onTile in 12..19) {
                ObjectAnimator.ofFloat(characterAvatar, "translationY", locationY, locationY - 160f).apply {
                    duration = 250
                    start()
                }
                // Updates the location of the player vertically, in the opposite direction of the first if statement
                locationY -= 160f
                Log.d(TAG, "Moved up")
            }

            // Increments the tile count to reflect where the player is now on screen
            onTile++
            Log.d(TAG, "onTile: " + onTile)

            // Once onTile is greater than 19, the player is already shown to be on the 20th tile, so the game ends there and the finishing function is called
            if (onTile > 19) {
                Toast.makeText(this@GameActivity, "You win!", Toast.LENGTH_SHORT).show()
                gameFinished()
            }
        }
    }

    // Creates an instance of QuestionFragment which creates an alertdialog with a multiple choice question
    private fun showQuestionDialog() {
        QuestionFragment().show(supportFragmentManager, "QuestionFragment")
    }

    // Function that handles behavior on when a choice in the alertdialog is clicked
    // If the correct answer was selected, coinsEarned is incremented and the visual count is updated to reflect that
    // The movePlayer() function is also called
    // If the incorrect answer was selected, only a toast is shown saying so
    override fun onDialogItemClick(boolean: Boolean) {
        if (boolean) {
            coinsEarned++
            coinsCount.text = "Coins Earned: $coinsEarned"
            movePlayer()
        } else
            Toast.makeText(this@GameActivity, "The answer is not correct", Toast.LENGTH_LONG).show()
    }

    // Last function of the game that creates an intent with the number of coins collected to return to the main activity
    private fun gameFinished() {
        val intent = Intent()
        intent.putExtra("Coins", coinsEarned)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    companion object {
        val TAG = GameActivity::class.java.simpleName
    }
}