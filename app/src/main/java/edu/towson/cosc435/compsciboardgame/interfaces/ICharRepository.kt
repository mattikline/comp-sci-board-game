package edu.towson.cosc435.compsciboardgame.interfaces

import edu.towson.cosc435.compsciboardgame.model.Chars

// Interface for the repository, has potential for adding your own characters
interface ICharRepository {
    fun getCount(): Int
    fun getChar(idx: Int): Chars
    suspend fun getAll(): List<Chars>
    suspend fun remove(char: Chars)
    suspend fun replace(idx: Int, char: Chars)
    suspend fun addChar(char: Chars)
}