package edu.towson.cosc435.compsciboardgame.database

import android.content.Context
import androidx.room.Room
import edu.towson.cosc435.compsciboardgame.R
import edu.towson.cosc435.compsciboardgame.interfaces.ICharRepository
import edu.towson.cosc435.compsciboardgame.model.Chars
import java.util.*

// Creates a repository for accessing the database using the repository interface we defined
class CharDatabaseRepository(ctx: Context) : ICharRepository {
    private val charList: MutableList<Chars> = mutableListOf()
    private val db: CharDatabase

    // Initializes some preset entries
    init {
        val ada = Chars(
            UUID.randomUUID(),
            "Ada Lovelace",
            R.drawable.adalovelace,
            "World's first computer programmer."
        )
        charList.add(ada)

        val robot = Chars(
            UUID.randomUUID(),
            "A Robot",
            R.drawable.robotchar,
            "A friendly robot."
        )
        charList.add(robot)

        val billgates = Chars(
            UUID.randomUUID(),
            "Bill Gates",
            R.drawable.billgates,
            "Founder of Microsoft."
        )
        charList.add(billgates)

        val stevejobs = Chars(
            UUID.randomUUID(),
            "Steve Jobes",
            R.drawable.stevejobs,
            "Founder of Apple."
        )
        charList.add(stevejobs)

        val titan = Chars(
            UUID.randomUUID(),
            "A titan",
            R.drawable.titan,
            "A giant titan."
        )
        charList.add(titan)

        val blacksmith = Chars(
            UUID.randomUUID(),
            "A blacksmith",
            R.drawable.blacksmith,
            "A working blacksmith."
        )
        charList.add(blacksmith)

        // Builds the database
        db = Room.databaseBuilder(ctx,
            CharDatabase::class.java,
            "characters.db").build()
    }

    // Gets the size of the list of characters
    override fun getCount(): Int {
        return charList.size
    }

    // Gets a specific character at the index
    override fun getChar(idx: Int): Chars {
        return charList.get(idx)
    }

    // Gets all the characters in the list
    override suspend fun getAll(): List<Chars> {
        if (charList.size == 0) {
            refreshCharList()
        }
        return charList
    }

    // Removes a character from the database
    override suspend fun remove(char: Chars) {
        db.charDao().deleteChar(char)
        refreshCharList()
    }

    // Replaces a character in the database at the index
    override suspend fun replace(idx: Int, char: Chars) {
        db.charDao().updateChar(char)
        refreshCharList()
    }

    // Adds a character to the database
    override suspend fun addChar(char: Chars) {
        db.charDao().addChar(char)
        refreshCharList()
    }

    // Helper function for clearing the mutableList and re-adding everything from the database
    private suspend fun refreshCharList() {
        charList.clear()
        val chars = db.charDao().getAllChars()
        charList.addAll(chars)
    }
}