# Comp Sci Board Game

This project is a board game app for android where the player answers computer science questions. The app is written in kotlin and made using android studio.

## Purpose

- Computer science students need to memorize some computer science vocabulary.
- The best way is to practice through answering quick questions.
- Our app makes it fun by making it part of a board game.
- Move forward when you get correct answers and earn coins to show how many you�ve answered.

## How To Play

1. Select a character.
2. Select a board to play on.
3. Click the question button to answer random questions.
4. Move forward for getting correct answers.
5. Earn coins for correct answers, shows how many you've answered in total.

## Used Technologies

- RecyclerView
- Database
- Fragments
- AlertDialog
- ObjectAnimator
- Unit tests recorded with Espresso